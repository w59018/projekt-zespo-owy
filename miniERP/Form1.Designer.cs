﻿namespace miniERP
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.bPz = new System.Windows.Forms.Button();
            this.bOdbiorcy = new System.Windows.Forms.Button();
            this.bFv = new System.Windows.Forms.Button();
            this.bTowary = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bPz
            // 
            this.bPz.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bPz.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bPz.Location = new System.Drawing.Point(135, 37);
            this.bPz.Name = "bPz";
            this.bPz.Size = new System.Drawing.Size(300, 200);
            this.bPz.TabIndex = 0;
            this.bPz.Text = "Dokumenty PZ";
            this.bPz.UseVisualStyleBackColor = false;
            this.bPz.Click += new System.EventHandler(this.bPz_Click);
            // 
            // bOdbiorcy
            // 
            this.bOdbiorcy.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bOdbiorcy.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bOdbiorcy.Location = new System.Drawing.Point(135, 285);
            this.bOdbiorcy.Name = "bOdbiorcy";
            this.bOdbiorcy.Size = new System.Drawing.Size(300, 200);
            this.bOdbiorcy.TabIndex = 1;
            this.bOdbiorcy.Text = "Kontrahenci";
            this.bOdbiorcy.UseVisualStyleBackColor = false;
            this.bOdbiorcy.Click += new System.EventHandler(this.bOdbiorcy_Click);
            // 
            // bFv
            // 
            this.bFv.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bFv.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bFv.Location = new System.Drawing.Point(526, 37);
            this.bFv.Name = "bFv";
            this.bFv.Size = new System.Drawing.Size(300, 200);
            this.bFv.TabIndex = 2;
            this.bFv.Text = "Dokumenty FV";
            this.bFv.UseVisualStyleBackColor = false;
            this.bFv.Click += new System.EventHandler(this.bFv_Click);
            // 
            // bTowary
            // 
            this.bTowary.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bTowary.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bTowary.Location = new System.Drawing.Point(526, 285);
            this.bTowary.Name = "bTowary";
            this.bTowary.Size = new System.Drawing.Size(300, 200);
            this.bTowary.TabIndex = 3;
            this.bTowary.Text = "Towary";
            this.bTowary.UseVisualStyleBackColor = false;
            this.bTowary.Click += new System.EventHandler(this.bTowary_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 535);
            this.Controls.Add(this.bTowary);
            this.Controls.Add(this.bFv);
            this.Controls.Add(this.bOdbiorcy);
            this.Controls.Add(this.bPz);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "miniERP";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bPz;
        private System.Windows.Forms.Button bOdbiorcy;
        private System.Windows.Forms.Button bFv;
        private System.Windows.Forms.Button bTowary;
    }
}

