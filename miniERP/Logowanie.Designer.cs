﻿namespace miniERP
{
    partial class Logowanie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Logowanie));
            this.label1 = new System.Windows.Forms.Label();
            this.tb_Serwer = new System.Windows.Forms.TextBox();
            this.tb_Baza = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_Login = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_Haslo = new System.Windows.Forms.MaskedTextBox();
            this.b_Zaloguj = new System.Windows.Forms.Button();
            this.dokdostpoz_tempTableAdapter1 = new miniERP.w59018DataSet1TableAdapters.dokdostpoz_tempTableAdapter();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Adres wraz z instancją serwera";
            // 
            // tb_Serwer
            // 
            this.tb_Serwer.Location = new System.Drawing.Point(70, 62);
            this.tb_Serwer.Name = "tb_Serwer";
            this.tb_Serwer.Size = new System.Drawing.Size(346, 20);
            this.tb_Serwer.TabIndex = 1;
            this.tb_Serwer.Text = "KAROLEK\\KAROL";
            // 
            // tb_Baza
            // 
            this.tb_Baza.Location = new System.Drawing.Point(70, 109);
            this.tb_Baza.Name = "tb_Baza";
            this.tb_Baza.Size = new System.Drawing.Size(346, 20);
            this.tb_Baza.TabIndex = 3;
            this.tb_Baza.Text = "w59018";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Baza danych";
            // 
            // tb_Login
            // 
            this.tb_Login.Location = new System.Drawing.Point(70, 155);
            this.tb_Login.Name = "tb_Login";
            this.tb_Login.Size = new System.Drawing.Size(346, 20);
            this.tb_Login.TabIndex = 5;
            this.tb_Login.Text = "sa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(67, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Login";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(67, 185);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Hasło";
            // 
            // tb_Haslo
            // 
            this.tb_Haslo.Location = new System.Drawing.Point(70, 202);
            this.tb_Haslo.Name = "tb_Haslo";
            this.tb_Haslo.PasswordChar = '*';
            this.tb_Haslo.Size = new System.Drawing.Size(346, 20);
            this.tb_Haslo.TabIndex = 7;
            this.tb_Haslo.Text = "503415528";
            // 
            // b_Zaloguj
            // 
            this.b_Zaloguj.Location = new System.Drawing.Point(296, 252);
            this.b_Zaloguj.Name = "b_Zaloguj";
            this.b_Zaloguj.Size = new System.Drawing.Size(119, 38);
            this.b_Zaloguj.TabIndex = 8;
            this.b_Zaloguj.Text = "Zaloguj";
            this.b_Zaloguj.UseVisualStyleBackColor = true;
            this.b_Zaloguj.Click += new System.EventHandler(this.b_Zaloguj_Click);
            // 
            // dokdostpoz_tempTableAdapter1
            // 
            this.dokdostpoz_tempTableAdapter1.ClearBeforeFill = true;
            // 
            // Logowanie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 366);
            this.Controls.Add(this.b_Zaloguj);
            this.Controls.Add(this.tb_Haslo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_Login);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_Baza);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_Serwer);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Logowanie";
            this.Text = "Logowanie";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_Serwer;
        private System.Windows.Forms.TextBox tb_Baza;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_Login;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox tb_Haslo;
        private System.Windows.Forms.Button b_Zaloguj;
        private w59018DataSet1TableAdapters.dokdostpoz_tempTableAdapter dokdostpoz_tempTableAdapter1;
    }
}