﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace miniERP
{
    public partial class Logowanie : Form
    {
        PolaczenieSQL pol = new PolaczenieSQL();

        public static string serwer = "";
        public static string baza = "";
        public static string login = "";
        public static string haslo = "";

        public Logowanie()
        {
            InitializeComponent();
        }

        private void b_Zaloguj_Click(object sender, EventArgs e)
        {
            serwer = tb_Serwer.Text;
            baza = tb_Baza.Text;
            login = tb_Login.Text;
            haslo = tb_Haslo.Text;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                pol.polaczenieSQL();
                Cursor.Current = Cursors.Default;
                Close();
            }
            catch
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Błąd logowania SQL");
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            serwer = tb_Serwer.Text;
            baza = tb_Baza.Text;
            login = tb_Login.Text;
            haslo = tb_Haslo.Text;
            try
            {

                Cursor.Current = Cursors.WaitCursor;
                pol.polaczenieSQL();                
                pol.cmd = new SqlCommand(Skrypt.skrypt, pol.con);
                MessageBox.Show(Skrypt.skrypt);
                pol.cmd.CommandType = CommandType.Text;
                pol.cmd.ExecuteNonQuery();
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Utworzono bazę danych!");


            }
            catch (Exception)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Błąd połączenia SQL!");

            }
            
        }
    }
}
