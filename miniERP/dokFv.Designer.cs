﻿namespace miniERP
{
    partial class dokFv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dokFv));
            this.button1 = new System.Windows.Forms.Button();
            this.tbIdentyfikator = new System.Windows.Forms.TextBox();
            this.tbWartDok = new System.Windows.Forms.TextBox();
            this.Identyfikator = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.doksprzedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.w59018DataSet = new miniERP.w59018DataSet();
            this.doksprzedTableAdapter = new miniERP.w59018DataSetTableAdapters.doksprzedTableAdapter();
            this.LabelOdbiorca = new System.Windows.Forms.Label();
            this.tbOdbiorca = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.doksprzedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(459, 48);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 53);
            this.button1.TabIndex = 20;
            this.button1.Text = "NOWA FV";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbIdentyfikator
            // 
            this.tbIdentyfikator.Location = new System.Drawing.Point(161, 81);
            this.tbIdentyfikator.Name = "tbIdentyfikator";
            this.tbIdentyfikator.Size = new System.Drawing.Size(208, 20);
            this.tbIdentyfikator.TabIndex = 21;
            // 
            // tbWartDok
            // 
            this.tbWartDok.Location = new System.Drawing.Point(161, 114);
            this.tbWartDok.Name = "tbWartDok";
            this.tbWartDok.Size = new System.Drawing.Size(100, 20);
            this.tbWartDok.TabIndex = 22;
            // 
            // Identyfikator
            // 
            this.Identyfikator.AutoSize = true;
            this.Identyfikator.Location = new System.Drawing.Point(82, 84);
            this.Identyfikator.Name = "Identyfikator";
            this.Identyfikator.Size = new System.Drawing.Size(65, 13);
            this.Identyfikator.TabIndex = 24;
            this.Identyfikator.Text = "Identyfikator";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(82, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Wartość";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(21, 200);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1019, 275);
            this.dataGridView1.TabIndex = 27;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.otworzFv);
            // 
            // doksprzedBindingSource
            // 
            this.doksprzedBindingSource.DataMember = "doksprzed";
            this.doksprzedBindingSource.DataSource = this.w59018DataSet;
            // 
            // w59018DataSet
            // 
            this.w59018DataSet.DataSetName = "w59018DataSet";
            this.w59018DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // doksprzedTableAdapter
            // 
            this.doksprzedTableAdapter.ClearBeforeFill = true;
            // 
            // LabelOdbiorca
            // 
            this.LabelOdbiorca.AutoSize = true;
            this.LabelOdbiorca.Location = new System.Drawing.Point(82, 51);
            this.LabelOdbiorca.Name = "LabelOdbiorca";
            this.LabelOdbiorca.Size = new System.Drawing.Size(50, 13);
            this.LabelOdbiorca.TabIndex = 29;
            this.LabelOdbiorca.Text = "Odbiorca";
            // 
            // tbOdbiorca
            // 
            this.tbOdbiorca.Location = new System.Drawing.Point(161, 48);
            this.tbOdbiorca.Name = "tbOdbiorca";
            this.tbOdbiorca.Size = new System.Drawing.Size(208, 20);
            this.tbOdbiorca.TabIndex = 28;
            // 
            // dokFv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 535);
            this.Controls.Add(this.LabelOdbiorca);
            this.Controls.Add(this.tbOdbiorca);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Identyfikator);
            this.Controls.Add(this.tbWartDok);
            this.Controls.Add(this.tbIdentyfikator);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "dokFv";
            this.Text = "Lista dokumentów FV";
            this.Load += new System.EventHandler(this.dokFv_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.doksprzedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbIdentyfikator;
        private System.Windows.Forms.TextBox tbWartDok;
        private System.Windows.Forms.Label Identyfikator;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private w59018DataSet w59018DataSet;
        private System.Windows.Forms.BindingSource doksprzedBindingSource;
        private w59018DataSetTableAdapters.doksprzedTableAdapter doksprzedTableAdapter;
        private System.Windows.Forms.Label LabelOdbiorca;
        private System.Windows.Forms.TextBox tbOdbiorca;
    }
}