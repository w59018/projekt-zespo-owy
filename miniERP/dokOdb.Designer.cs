﻿namespace miniERP
{
    partial class dokOdb
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dokOdb));
            this.bDodaj = new System.Windows.Forms.Button();
            this.tbNazwa = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbKod = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbPoczta = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbUlica = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbPowiat = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbNr = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbGmina = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbRabat = new System.Windows.Forms.TextBox();
            this.listaOdb = new System.Windows.Forms.DataGridView();
            this.odbiorca_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.odbiorcaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.w59018DataSet = new miniERP.w59018DataSet();
            this.fillByToolStrip = new System.Windows.Forms.ToolStrip();
            this.fillByToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.odbiorcaTableAdapter = new miniERP.w59018DataSetTableAdapters.odbiorcaTableAdapter();
            this.bUsun = new System.Windows.Forms.Button();
            this.w59018DataSet1 = new miniERP.w59018DataSet();
            this.bZmien = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.listaOdb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.odbiorcaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet)).BeginInit();
            this.fillByToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // bDodaj
            // 
            this.bDodaj.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bDodaj.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.bDodaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.bDodaj.Location = new System.Drawing.Point(594, 30);
            this.bDodaj.Name = "bDodaj";
            this.bDodaj.Size = new System.Drawing.Size(121, 53);
            this.bDodaj.TabIndex = 0;
            this.bDodaj.Text = "Dodaj";
            this.bDodaj.UseVisualStyleBackColor = false;
            this.bDodaj.Click += new System.EventHandler(this.bDodaj_Click);
            // 
            // tbNazwa
            // 
            this.tbNazwa.Location = new System.Drawing.Point(115, 30);
            this.tbNazwa.Name = "tbNazwa";
            this.tbNazwa.Size = new System.Drawing.Size(437, 20);
            this.tbNazwa.TabIndex = 1;
            this.tbNazwa.TextChanged += new System.EventHandler(this.tbNazwa_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(9, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Imię / Nazwisko";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(18, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Kod Pocztowy";
            // 
            // tbKod
            // 
            this.tbKod.Location = new System.Drawing.Point(115, 56);
            this.tbKod.Name = "tbKod";
            this.tbKod.Size = new System.Drawing.Size(64, 20);
            this.tbKod.TabIndex = 3;
            this.tbKod.TextChanged += new System.EventHandler(this.tbKod_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(192, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Poczta";
            // 
            // tbPoczta
            // 
            this.tbPoczta.Location = new System.Drawing.Point(246, 56);
            this.tbPoczta.Name = "tbPoczta";
            this.tbPoczta.Size = new System.Drawing.Size(306, 20);
            this.tbPoczta.TabIndex = 5;
            this.tbPoczta.TextChanged += new System.EventHandler(this.tbPoczta_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(71, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Ulica";
            // 
            // tbUlica
            // 
            this.tbUlica.Location = new System.Drawing.Point(115, 82);
            this.tbUlica.Name = "tbUlica";
            this.tbUlica.Size = new System.Drawing.Size(270, 20);
            this.tbUlica.TabIndex = 7;
            this.tbUlica.TextChanged += new System.EventHandler(this.tbUlica_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(62, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Powiat";
            // 
            // tbPowiat
            // 
            this.tbPowiat.Location = new System.Drawing.Point(115, 108);
            this.tbPowiat.Name = "tbPowiat";
            this.tbPowiat.Size = new System.Drawing.Size(157, 20);
            this.tbPowiat.TabIndex = 9;
            this.tbPowiat.TextChanged += new System.EventHandler(this.tbPowiat_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(403, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Nr Domu";
            // 
            // tbNr
            // 
            this.tbNr.Location = new System.Drawing.Point(470, 82);
            this.tbNr.Name = "tbNr";
            this.tbNr.Size = new System.Drawing.Size(82, 20);
            this.tbNr.TabIndex = 11;
            this.tbNr.TextChanged += new System.EventHandler(this.tbNr_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(286, 110);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Gmina";
            // 
            // tbGmina
            // 
            this.tbGmina.Location = new System.Drawing.Point(340, 108);
            this.tbGmina.Name = "tbGmina";
            this.tbGmina.Size = new System.Drawing.Size(212, 20);
            this.tbGmina.TabIndex = 13;
            this.tbGmina.TextChanged += new System.EventHandler(this.tbGmina_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(65, 135);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 16);
            this.label8.TabIndex = 16;
            this.label8.Text = "Rabat";
            // 
            // tbRabat
            // 
            this.tbRabat.Location = new System.Drawing.Point(115, 134);
            this.tbRabat.Name = "tbRabat";
            this.tbRabat.Size = new System.Drawing.Size(64, 20);
            this.tbRabat.TabIndex = 15;
            this.tbRabat.TextChanged += new System.EventHandler(this.tbRabat_TextChanged);
            // 
            // listaOdb
            // 
            this.listaOdb.AllowUserToAddRows = false;
            this.listaOdb.AllowUserToDeleteRows = false;
            this.listaOdb.AllowUserToResizeColumns = false;
            this.listaOdb.AllowUserToResizeRows = false;
            this.listaOdb.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.listaOdb.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.listaOdb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listaOdb.CausesValidation = false;
            this.listaOdb.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.listaOdb.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.listaOdb.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaOdb.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.odbiorca_id});
            this.listaOdb.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.listaOdb.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.listaOdb.Location = new System.Drawing.Point(12, 174);
            this.listaOdb.Margin = new System.Windows.Forms.Padding(0);
            this.listaOdb.Name = "listaOdb";
            this.listaOdb.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.listaOdb.RowHeadersVisible = false;
            this.listaOdb.RowHeadersWidth = 35;
            this.listaOdb.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.listaOdb.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listaOdb.Size = new System.Drawing.Size(978, 287);
            this.listaOdb.TabIndex = 18;
            this.listaOdb.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.listaOdb_CellContentClick);
            // 
            // odbiorca_id
            // 
            this.odbiorca_id.DataPropertyName = "odbiorca_id";
            this.odbiorca_id.HeaderText = "odbiorca_id";
            this.odbiorca_id.Name = "odbiorca_id";
            this.odbiorca_id.ReadOnly = true;
            this.odbiorca_id.Visible = false;
            this.odbiorca_id.Width = 87;
            // 
            // odbiorcaBindingSource
            // 
            this.odbiorcaBindingSource.DataMember = "odbiorca";
            this.odbiorcaBindingSource.DataSource = this.w59018DataSet;
            // 
            // w59018DataSet
            // 
            this.w59018DataSet.DataSetName = "w59018DataSet";
            this.w59018DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fillByToolStrip
            // 
            this.fillByToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fillByToolStripButton});
            this.fillByToolStrip.Location = new System.Drawing.Point(0, 0);
            this.fillByToolStrip.Name = "fillByToolStrip";
            this.fillByToolStrip.Size = new System.Drawing.Size(1063, 25);
            this.fillByToolStrip.TabIndex = 19;
            this.fillByToolStrip.Text = "fillByToolStrip";
            this.fillByToolStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.fillByToolStrip_ItemClicked);
            // 
            // fillByToolStripButton
            // 
            this.fillByToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fillByToolStripButton.Name = "fillByToolStripButton";
            this.fillByToolStripButton.Size = new System.Drawing.Size(48, 22);
            this.fillByToolStripButton.Text = "Wstecz";
            this.fillByToolStripButton.Click += new System.EventHandler(this.fillByToolStripButton_Click);
            // 
            // odbiorcaTableAdapter
            // 
            this.odbiorcaTableAdapter.ClearBeforeFill = true;
            // 
            // bUsun
            // 
            this.bUsun.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bUsun.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.bUsun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.bUsun.Location = new System.Drawing.Point(721, 30);
            this.bUsun.Name = "bUsun";
            this.bUsun.Size = new System.Drawing.Size(121, 53);
            this.bUsun.TabIndex = 20;
            this.bUsun.Text = "Usuń";
            this.bUsun.UseVisualStyleBackColor = false;
            this.bUsun.Click += new System.EventHandler(this.bUsun_Click);
            // 
            // w59018DataSet1
            // 
            this.w59018DataSet1.DataSetName = "w59018DataSet";
            this.w59018DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bZmien
            // 
            this.bZmien.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bZmien.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.bZmien.Location = new System.Drawing.Point(848, 30);
            this.bZmien.Name = "bZmien";
            this.bZmien.Size = new System.Drawing.Size(121, 53);
            this.bZmien.TabIndex = 21;
            this.bZmien.Text = "Aktualizuj dane";
            this.bZmien.UseVisualStyleBackColor = false;
            this.bZmien.Click += new System.EventHandler(this.bZmien_Click);
            // 
            // dokOdb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1063, 535);
            this.Controls.Add(this.bZmien);
            this.Controls.Add(this.bUsun);
            this.Controls.Add(this.fillByToolStrip);
            this.Controls.Add(this.listaOdb);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbRabat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbGmina);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbNr);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbPowiat);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbUlica);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbPoczta);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbKod);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbNazwa);
            this.Controls.Add(this.bDodaj);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "dokOdb";
            this.Text = "Lista Kontrahentów";
            this.Load += new System.EventHandler(this.dokOdb_Load);
            ((System.ComponentModel.ISupportInitialize)(this.listaOdb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.odbiorcaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet)).EndInit();
            this.fillByToolStrip.ResumeLayout(false);
            this.fillByToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button bDodaj;
        private System.Windows.Forms.TextBox tbNazwa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbKod;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbPoczta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbUlica;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbPowiat;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbNr;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbGmina;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbRabat;
        private w59018DataSet w59018DataSet;
        private System.Windows.Forms.BindingSource odbiorcaBindingSource;
        private w59018DataSetTableAdapters.odbiorcaTableAdapter odbiorcaTableAdapter;
        private System.Windows.Forms.ToolStrip fillByToolStrip;
        private System.Windows.Forms.ToolStripButton fillByToolStripButton;
        internal System.Windows.Forms.Button bUsun;
        protected System.Windows.Forms.DataGridView listaOdb;
        private w59018DataSet w59018DataSet1;
        private System.Windows.Forms.Button bZmien;
        private System.Windows.Forms.DataGridViewTextBoxColumn odbiorca_id;
    }
}