﻿namespace miniERP
{
    partial class dokPz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dokPz));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tbIdentyfikator = new System.Windows.Forms.TextBox();
            this.tbWartDok = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dokdostBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.w59018DataSet1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.w59018DataSet1 = new miniERP.w59018DataSet();
            this.dokdostTableAdapter = new miniERP.w59018DataSetTableAdapters.dokdostTableAdapter();
            this.tbDostawca = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dokdostBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Identyfikator";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Wart. Dok.";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(605, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 53);
            this.button1.TabIndex = 4;
            this.button1.Text = "NOWY PZ";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbIdentyfikator
            // 
            this.tbIdentyfikator.Location = new System.Drawing.Point(143, 65);
            this.tbIdentyfikator.Name = "tbIdentyfikator";
            this.tbIdentyfikator.Size = new System.Drawing.Size(193, 20);
            this.tbIdentyfikator.TabIndex = 6;
            this.tbIdentyfikator.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // tbWartDok
            // 
            this.tbWartDok.Location = new System.Drawing.Point(143, 95);
            this.tbWartDok.Name = "tbWartDok";
            this.tbWartDok.Size = new System.Drawing.Size(72, 20);
            this.tbWartDok.TabIndex = 7;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.Location = new System.Drawing.Point(27, 190);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(974, 312);
            this.dataGridView1.TabIndex = 10;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick_1);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.otworz_PZ);
            // 
            // dokdostBindingSource
            // 
            this.dokdostBindingSource.DataMember = "dokdost";
            this.dokdostBindingSource.DataSource = this.w59018DataSet1BindingSource;
            // 
            // w59018DataSet1BindingSource
            // 
            this.w59018DataSet1BindingSource.DataSource = this.w59018DataSet1;
            this.w59018DataSet1BindingSource.Position = 0;
            // 
            // w59018DataSet1
            // 
            this.w59018DataSet1.DataSetName = "w59018DataSet";
            this.w59018DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dokdostTableAdapter
            // 
            this.dokdostTableAdapter.ClearBeforeFill = true;
            // 
            // tbDostawca
            // 
            this.tbDostawca.Location = new System.Drawing.Point(143, 32);
            this.tbDostawca.Name = "tbDostawca";
            this.tbDostawca.Size = new System.Drawing.Size(193, 20);
            this.tbDostawca.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(56, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Dostawca";
            // 
            // dokPz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 535);
            this.Controls.Add(this.tbDostawca);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.tbWartDok);
            this.Controls.Add(this.tbIdentyfikator);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "dokPz";
            this.Text = "Lista dokumentów PZ";
            this.Load += new System.EventHandler(this.nowyPz_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dokdostBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbIdentyfikator;
        private System.Windows.Forms.TextBox tbWartDok;
        private w59018DataSet w59018DataSet1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource w59018DataSet1BindingSource;
        private System.Windows.Forms.BindingSource dokdostBindingSource;
        private w59018DataSetTableAdapters.dokdostTableAdapter dokdostTableAdapter;
        private System.Windows.Forms.TextBox tbDostawca;
        private System.Windows.Forms.Label label3;
    }
}