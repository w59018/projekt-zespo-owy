﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;

namespace miniERP
{
    /// <summary>
    ///  Klasa dokumentów zakupu PZ.
    /// </summary>
    public partial class dokPz : Form
    {
        PolaczenieSQL pol = new PolaczenieSQL();
        string id;

        public dokPz()
        {
            InitializeComponent();
        }

        ///<summary>
        /// Inicjalizowanie połączenia i napełnianie formatki danymi
        ///</summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void nowyPz_Load(object sender, EventArgs e)
        {
            // TODO: Ten wiersz kodu wczytuje dane do tabeli 'w59018DataSet1.dokdost' . Możesz go przenieść lub usunąć.
            try
            {
                laduj_dane();
            }
            catch
            {

            }

        }

        public void laduj_dane()
        {
            pol.polaczenieSQL();
            SqlDataAdapter da = new SqlDataAdapter("SELECT dokdost_id, o.nazwa, dok_idm, datdok, wartdok, uwagi FROM dokdost dz LEFT JOIN odbiorca o ON o.odbiorca_id = dz.dostawca_id", pol.con);
            DataTable lista = new DataTable();
            da.Fill(lista);
            dataGridView1.DataSource = lista;

            //DataSet ds = new DataSet();
            //da.Fill(ds, "wytwor");
            //listaTow.DataSource = ds;
            //listaTow.DataMember = "wytwor";
            pol.con.Close();

        }

        private void listaPzToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.dokdostTableAdapter.listaPz(this.w59018DataSet1.dokdost);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        /// <summary>
        /// Metoda odpowiedzialna za utworzenie nowego dokumentu PZ po wciśnięciu przycisku dodaj
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void button1_Click(object sender, EventArgs e)
        {
            pol.polaczenieSQL();
            string query = "DELETE FROM dokdostpoz_temp";
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            try
            {
                pol.cmd.ExecuteNonQuery();
            }
            catch (SqlException blad)
            {
                if (blad.Errors.Count > 0)
                {
                    MessageBox.Show("Nie można usunąć pozycji!");
                    return;
                }

            }
                                  
            pol.zakpolaczenieSQL();

            this.Hide();
            nowyPZ nowyPz = new nowyPZ();
            nowyPz.ShowDialog();

        }


        /// <summary>
        /// Metoda odpowiedzialna za pobranie danych z aktualnie zaznaczonej pozycji na liście dokumentów PZ
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void dataGridView1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            
            DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
            id = row.Cells["dokdost_id"].Value.ToString();
            pol.polaczenieSQL();
            string query = "SELECT nazwa FROM odbiorca o INNER JOIN dokdost d ON d.dostawca_id = o.odbiorca_id WHERE d.dokdost_id=" + id;
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            pol.cmd.ExecuteNonQuery();
            string zwrotka = (string)pol.cmd.ExecuteScalar();
            tbDostawca.Text = zwrotka;
            tbIdentyfikator.Text = row.Cells["dok_idm"].Value.ToString();
            tbWartDok.Text = row.Cells["wartdok"].Value.ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void otworz_PZ(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
            id = row.Cells["dokdost_id"].Value.ToString();
            pol.polaczenieSQL();
            string query = "EXEC up_kpi_otworz_dok_pz " + id;
            //MessageBox.Show(query)
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            try
            {
                pol.cmd.ExecuteNonQuery();
            }
            catch (SqlException blad)
            {
                if (blad.Errors.Count > 0)
                {
                    MessageBox.Show("Nie można otworzyć dokumentu!");
                    return;
                }

            }

            this.Hide();
            pPZ pPz = new pPZ();
            pPz.ShowDialog();
        }
    }
}
