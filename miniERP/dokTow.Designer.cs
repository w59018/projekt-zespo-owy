﻿namespace miniERP
{
    partial class dokTow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dokTow));
            this.wytworBindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.w59018DataSet4 = new miniERP.w59018DataSet4();
            this.Identyfikator = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbIdentyfikator = new System.Windows.Forms.TextBox();
            this.tbNazwa = new System.Windows.Forms.TextBox();
            this.tbCena = new System.Windows.Forms.TextBox();
            this.tbUwagi = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tbNazwaRozw = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.w59018DataSet3 = new miniERP.w59018DataSet3();
            this.w59018DataSet3BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wytworBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.wytworTableAdapter1 = new miniERP.w59018DataSet3TableAdapters.wytworTableAdapter();
            this.wytworBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.w59018DataSet = new miniERP.w59018DataSet();
            this.wytworTableAdapter = new miniERP.w59018DataSetTableAdapters.wytworTableAdapter();
            this.wytworBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.w59018DataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wytworBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.wytworTableAdapter2 = new miniERP.w59018DataSet4TableAdapters.wytworTableAdapter();
            this.w59018DataSet5 = new miniERP.w59018DataSet5();
            this.wytworBindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.wytworTableAdapter3 = new miniERP.w59018DataSet5TableAdapters.wytworTableAdapter();
            this.listaTow = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.wytworBindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet3BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wytworBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wytworBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wytworBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wytworBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wytworBindingSource5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listaTow)).BeginInit();
            this.SuspendLayout();
            // 
            // wytworBindingSource4
            // 
            this.wytworBindingSource4.DataMember = "wytwor";
            this.wytworBindingSource4.DataSource = this.w59018DataSet4;
            // 
            // w59018DataSet4
            // 
            this.w59018DataSet4.DataSetName = "w59018DataSet4";
            this.w59018DataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // Identyfikator
            // 
            this.Identyfikator.AutoSize = true;
            this.Identyfikator.Location = new System.Drawing.Point(25, 28);
            this.Identyfikator.Name = "Identyfikator";
            this.Identyfikator.Size = new System.Drawing.Size(65, 13);
            this.Identyfikator.TabIndex = 1;
            this.Identyfikator.Text = "Identyfikator";
            this.Identyfikator.Click += new System.EventHandler(this.dokTow_Load);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nazwa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Cena Sprzedaży";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Uwagi";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // tbIdentyfikator
            // 
            this.tbIdentyfikator.Location = new System.Drawing.Point(124, 24);
            this.tbIdentyfikator.Name = "tbIdentyfikator";
            this.tbIdentyfikator.Size = new System.Drawing.Size(100, 20);
            this.tbIdentyfikator.TabIndex = 5;
            this.tbIdentyfikator.TextChanged += new System.EventHandler(this.tbIdentyfikator_TextChanged);
            // 
            // tbNazwa
            // 
            this.tbNazwa.Location = new System.Drawing.Point(124, 56);
            this.tbNazwa.Name = "tbNazwa";
            this.tbNazwa.Size = new System.Drawing.Size(157, 20);
            this.tbNazwa.TabIndex = 6;
            // 
            // tbCena
            // 
            this.tbCena.Location = new System.Drawing.Point(124, 120);
            this.tbCena.Name = "tbCena";
            this.tbCena.Size = new System.Drawing.Size(73, 20);
            this.tbCena.TabIndex = 7;
            this.tbCena.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.keypress);
            // 
            // tbUwagi
            // 
            this.tbUwagi.Location = new System.Drawing.Point(124, 150);
            this.tbUwagi.Name = "tbUwagi";
            this.tbUwagi.Size = new System.Drawing.Size(225, 20);
            this.tbUwagi.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(503, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 53);
            this.button1.TabIndex = 9;
            this.button1.Text = "DODAJ TOWAR";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.button2.Location = new System.Drawing.Point(655, 28);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(121, 53);
            this.button2.TabIndex = 10;
            this.button2.Text = "USUŃ TOWAR";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tbNazwaRozw
            // 
            this.tbNazwaRozw.Location = new System.Drawing.Point(124, 88);
            this.tbNazwaRozw.Name = "tbNazwaRozw";
            this.tbNazwaRozw.Size = new System.Drawing.Size(225, 20);
            this.tbNazwaRozw.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Nazwa rozwinięta";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.button3.Location = new System.Drawing.Point(806, 28);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(125, 53);
            this.button3.TabIndex = 13;
            this.button3.Text = "AKTUALIZUJ";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // w59018DataSet3
            // 
            this.w59018DataSet3.DataSetName = "w59018DataSet3";
            this.w59018DataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // w59018DataSet3BindingSource
            // 
            this.w59018DataSet3BindingSource.DataSource = this.w59018DataSet3;
            this.w59018DataSet3BindingSource.Position = 0;
            // 
            // wytworBindingSource1
            // 
            this.wytworBindingSource1.DataMember = "wytwor";
            this.wytworBindingSource1.DataSource = this.w59018DataSet3BindingSource;
            // 
            // wytworTableAdapter1
            // 
            this.wytworTableAdapter1.ClearBeforeFill = true;
            // 
            // wytworBindingSource
            // 
            this.wytworBindingSource.DataMember = "wytwor";
            this.wytworBindingSource.DataSource = this.w59018DataSet;
            // 
            // w59018DataSet
            // 
            this.w59018DataSet.DataSetName = "w59018DataSet";
            this.w59018DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wytworTableAdapter
            // 
            this.wytworTableAdapter.ClearBeforeFill = true;
            // 
            // wytworBindingSource2
            // 
            this.wytworBindingSource2.DataMember = "wytwor";
            this.wytworBindingSource2.DataSource = this.w59018DataSet3BindingSource;
            // 
            // w59018DataSetBindingSource
            // 
            this.w59018DataSetBindingSource.DataSource = this.w59018DataSet;
            this.w59018DataSetBindingSource.Position = 0;
            // 
            // wytworBindingSource3
            // 
            this.wytworBindingSource3.DataMember = "wytwor";
            this.wytworBindingSource3.DataSource = this.w59018DataSetBindingSource;
            // 
            // wytworTableAdapter2
            // 
            this.wytworTableAdapter2.ClearBeforeFill = true;
            // 
            // w59018DataSet5
            // 
            this.w59018DataSet5.DataSetName = "w59018DataSet5";
            this.w59018DataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wytworBindingSource5
            // 
            this.wytworBindingSource5.DataMember = "wytwor";
            this.wytworBindingSource5.DataSource = this.w59018DataSet5;
            // 
            // wytworTableAdapter3
            // 
            this.wytworTableAdapter3.ClearBeforeFill = true;
            // 
            // listaTow
            // 
            this.listaTow.AllowUserToAddRows = false;
            this.listaTow.AllowUserToDeleteRows = false;
            this.listaTow.BackgroundColor = System.Drawing.Color.White;
            this.listaTow.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.listaTow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaTow.Location = new System.Drawing.Point(28, 197);
            this.listaTow.Name = "listaTow";
            this.listaTow.ReadOnly = true;
            this.listaTow.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listaTow.Size = new System.Drawing.Size(984, 326);
            this.listaTow.TabIndex = 0;
            this.listaTow.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.listaTow_CellContentClick);
            this.listaTow.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.listaTow_CellContentClick_2);
            // 
            // dokTow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 535);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.tbNazwaRozw);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbUwagi);
            this.Controls.Add(this.tbCena);
            this.Controls.Add(this.tbNazwa);
            this.Controls.Add(this.tbIdentyfikator);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Identyfikator);
            this.Controls.Add(this.listaTow);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "dokTow";
            this.Text = "Lista produktów";
            this.Load += new System.EventHandler(this.dokTow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.wytworBindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet3BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wytworBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wytworBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wytworBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wytworBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.w59018DataSet5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wytworBindingSource5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listaTow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private w59018DataSet w59018DataSet;
        private System.Windows.Forms.BindingSource wytworBindingSource;
        private w59018DataSetTableAdapters.wytworTableAdapter wytworTableAdapter;
        private System.Windows.Forms.Label Identyfikator;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbIdentyfikator;
        private System.Windows.Forms.TextBox tbNazwa;
        private System.Windows.Forms.TextBox tbCena;
        private System.Windows.Forms.TextBox tbUwagi;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox tbNazwaRozw;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.BindingSource w59018DataSet3BindingSource;
        private w59018DataSet3 w59018DataSet3;
        private System.Windows.Forms.BindingSource wytworBindingSource1;
        private w59018DataSet3TableAdapters.wytworTableAdapter wytworTableAdapter1;
        private System.Windows.Forms.BindingSource wytworBindingSource3;
        private System.Windows.Forms.BindingSource w59018DataSetBindingSource;
        private System.Windows.Forms.BindingSource wytworBindingSource2;
        private w59018DataSet4 w59018DataSet4;
        private System.Windows.Forms.BindingSource wytworBindingSource4;
        private w59018DataSet4TableAdapters.wytworTableAdapter wytworTableAdapter2;
        private w59018DataSet5 w59018DataSet5;
        private System.Windows.Forms.BindingSource wytworBindingSource5;
        private w59018DataSet5TableAdapters.wytworTableAdapter wytworTableAdapter3;
        protected System.Windows.Forms.DataGridView listaTow;
    }
}