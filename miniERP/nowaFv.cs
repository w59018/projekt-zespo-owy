﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;


namespace miniERP
{
    /// <summary>
    ///  Klasa tworzenia nowych dokumentów FV.
    /// </summary>
    public partial class nowaFv : Form
    {
        PolaczenieSQL pol = new PolaczenieSQL();
        string id;
        public int vis_button_off;


        /// <summary>
        /// Odswiezenie polaczenia SQL
        /// </summary>
        public void odswiezenie()
        {
            pol.polaczenieSQL();
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM doksprzedpoz_temp", pol.con);
            DataSet ds = new DataSet();
            da.Fill(ds, "wytwor_idm");
            doksprzedpoz_temp.DataSource = ds;
            doksprzedpoz_temp.DataMember = "wytwor_idm";
            pol.con.Close();

        }

        /// <summary>
        /// Okno z nowym dokumentem FV
        /// </summary>
        public nowaFv()
        {
            InitializeComponent();

            
            if (vis_button_off == 1)
            {
                button1.Visible = false;
            }
            
            LabelRabat.Text = "0,00";

            pol.polaczenieSQL();
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM odbiorca", pol.con);
            DataSet ds = new DataSet(); DataTable dt = new DataTable();
            da.Fill(dt);
            pol.con.Close();
            foreach (DataRow dr in dt.Rows)
            {
                comboBoxDostawca.Items.Add(dr["nazwa"].ToString());
            }


            pol.polaczenieSQL();
            SqlDataAdapter daa = new SqlDataAdapter("SELECT * FROM wytwor WHERE wytwor_idm != ''", pol.con);
            DataSet dss = new DataSet(); DataTable dtt = new DataTable();
            daa.Fill(dtt);
            pol.con.Close();
            foreach (DataRow drr in dtt.Rows)
            {
                comboBox2.Items.Add(drr["wytwor_idm"].ToString());
            }
        }

        private void LabelRabat_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Metoda pobierajaca wartosci do Comboboxa
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void comboBoxDostawca_SelectedIndexChanged(object sender, EventArgs e)
        {
            pol.polaczenieSQL();
            string query = "SELECT ISNULL(rabat,0.00) FROM odbiorca WHERE nazwa='" + comboBoxDostawca.Text + "'";
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            pol.cmd.ExecuteNonQuery();
            decimal zwrotka = (decimal)pol.cmd.ExecuteScalar();
            string rabat = zwrotka.ToString("#.000");
            LabelRabat.Text = rabat;
        }

        /// <summary>
        /// Metoda wybierajaca dane z zaznaczonego Comboboxa
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            pol.polaczenieSQL();
            string query = "SELECT ISNULL(censprzed,0.00) FROM wytwor WHERE wytwor_idm='" + comboBox2.Text + "'";
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            pol.cmd.ExecuteNonQuery();
            decimal zwrotka = (decimal)pol.cmd.ExecuteScalar();
            string rabat = zwrotka.ToString("#.000");
            tbCena.Text = rabat;
        }

        /// <summary>
        /// Metoda odpowiedzialna za wczytanie danych do formatki
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void nowaFv_Load(object sender, EventArgs e)
        {
            // TODO: Ten wiersz kodu wczytuje dane do tabeli 'w59018DataSet2.doksprzedpoz_temp' . Możesz go przenieść lub usunąć.
            try
            {
                laduj_dane();
            }
            catch
            {

            }

        }

        public void laduj_dane()
        {
            pol.polaczenieSQL();
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM doksprzedpoz_temp", pol.con);
            DataTable lista = new DataTable();
            da.Fill(lista);
            doksprzedpoz_temp.DataSource = lista;

            //DataSet ds = new DataSet();
            //da.Fill(ds, "wytwor");
            //listaTow.DataSource = ds;
            //listaTow.DataMember = "wytwor";
            pol.con.Close();

        }

        /// <summary>
        /// Przycisk dodawania pozycji na dokument
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void dodaj_Click(object sender, EventArgs e)
        {
            
            if (comboBox2.Text != "" && tbIlosc.Text != "" && tbCena.Text != "")
            {
                if (Convert.ToDecimal(tbCena.Text) == 0)
                {
                    string message = "NIE MOŻNA DODAĆ POZYCJI Z CENĄ 0,00!";
                    string caption = "DODANIE POZYCJI NIE MOŻLIWE";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result;
                    result = MessageBox.Show(message, caption, buttons);
                    return;
                }

                pol.polaczenieSQL();
                decimal ilosc = decimal.Parse(tbIlosc.Text);
                pol.cmd = new SqlCommand("SELECT ISNULL(ilosc,0) as ilosc FROM wytwor WHERE wytwor_idm ='" + comboBox2.Text +"'" , pol.con);
                pol.cmd.CommandType = CommandType.Text;
                decimal z_ilosc = Convert.ToDecimal(pol.cmd.ExecuteScalar());

                if (ilosc > z_ilosc)
                {
                    string message = "NIE MOŻNA DODAĆ POZYCJI!" + "\n" + "Podana ilość " + Convert.ToString(ilosc)  + "\n" + "Ilość dostępna na magazynie " + Convert.ToString(z_ilosc);
                    string caption = "DODANIE POZYCJI NIE MOŻLIWE";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result;
                    result = MessageBox.Show(message, caption, buttons);
                    return;
                }
                

                tbCena.Text = tbCena.Text.Replace(',', '.');
                pol.cmd = new SqlCommand("INSERT INTO doksprzedpoz_temp (wytwor_idm,ilosc,cena,cena_po_rabacie,uwagi) " +
                                 "VALUES	(@wytwor_idm,@ilosc,@cena,@cena_po_rabacie,@uwagi)", pol.con);
                pol.cmd.Parameters.AddWithValue("@wytwor_idm", comboBox2.Text);
                pol.cmd.Parameters.AddWithValue("@ilosc", tbIlosc.Text);
                pol.cmd.Parameters.AddWithValue("@cena", decimal.Parse(tbCena.Text, CultureInfo.InvariantCulture));
                pol.cmd.Parameters.AddWithValue("@uwagi", tbUwagi.Text);
                pol.cmd.Parameters.AddWithValue("@cena_po_rabacie", decimal.Parse(tbCena.Text, CultureInfo.InvariantCulture));
                pol.cmd.CommandType = CommandType.Text;
                pol.cmd.ExecuteNonQuery();

                //string message = "Dodano nową pozycję";
                //string caption = "Dodano pozycję";
                //MessageBoxButtons buttons = MessageBoxButtons.OK;
                //DialogResult result;

                comboBox2.Text = "";
                tbIlosc.Text = "";
                tbCena.Text = "";
                tbUwagi.Text = "";

                //result = MessageBox.Show(message, caption, buttons);

                odswiezenie();
            }
            else
            {
                string message = "Nie wypełniono wymaganych pól: Produkt, Ilość, Cena !";
                string caption = "UWAGA";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(message, caption, buttons);

            }
        }

        /// <summary>
        /// Pobieranie danych z aktualnie zaznaczonego rekordu na liscie 
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void doksprzedpoz_temp_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.doksprzedpoz_temp.Rows[e.RowIndex];
            id = row.Cells["doksprzedpoz_id"].Value.ToString();
        }

        /// <summary>
        /// Przycisk usuwania pozycji z faktury
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void btUsun_Click(object sender, EventArgs e)
        {
            pol.polaczenieSQL();
            string query = "DELETE FROM doksprzedpoz_temp WHERE doksprzedpoz_id = " + id;
            //MessageBox.Show(query);
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            try
            {
                pol.cmd.ExecuteNonQuery();
            }
            catch (SqlException blad)
            {
                if (blad.Errors.Count > 0)
                {
                    MessageBox.Show("Nie można usunąć pozycji!");
                    return;
                }

            }


            odswiezenie();
            pol.zakpolaczenieSQL();
        }

        /// <summary>
        /// Przycisk udzielania rabatu
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void button2_Click(object sender, EventArgs e)
        {
            string rabat = LabelRabat.Text;
            decimal d_rabat = System.Convert.ToDecimal(rabat);
            rabat = rabat.Replace(',', '.');

            if (d_rabat == 0)
            {
                return;
            }
            pol.polaczenieSQL();
            string query = "UPDATE doksprzedpoz_temp SET cena_po_rabacie = cena -  (cena / " + rabat + ")";
            //MessageBox.Show(query);
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            try
            {
                pol.cmd.ExecuteNonQuery();
            }
            catch (SqlException blad)
            {
                if (blad.Errors.Count > 0)
                {
                    MessageBox.Show("Nie można udzielić rabatu!");
                    return;
                }

            }


            odswiezenie();
            pol.zakpolaczenieSQL();
        }

        /// <summary>
        /// Przycisk anulowania udzielonego rabatu
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void btAnulujRabat_Click(object sender, EventArgs e)
        {
            pol.polaczenieSQL();
            string query = "UPDATE doksprzedpoz_temp SET cena_po_rabacie = cena ";
            //MessageBox.Show(query);
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            try
            {
                pol.cmd.ExecuteNonQuery();
            }
            catch (SqlException blad)
            {
                if (blad.Errors.Count > 0)
                {
                    MessageBox.Show("Nie można anulować udzielonego rabatu!");
                    return;
                }

            }


            odswiezenie();
            pol.zakpolaczenieSQL();
        }

        /// <summary>
        /// Przycisk i metoda zatwierdzajaca utworzony dokument sprzedazy
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void button1_Click(object sender, EventArgs e)
        {
            pol.polaczenieSQL();
            string query = "SELECT COUNT(*) FROM doksprzedpoz_temp";
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            pol.cmd.ExecuteNonQuery();
            int zwrotka = (Int32)pol.cmd.ExecuteScalar();

            if (comboBoxDostawca.Text == "")
            {
                MessageBox.Show("Brak dostawcy!");
                return;
            }

            if (zwrotka == 0)
            {
                MessageBox.Show("Brak pozycji na dokumencie!");
                return;
            }


            string dostawca = comboBoxDostawca.Text;
            string uwagi = tbOpisDokumentu.Text;
            string proc = "EXEC up_kpi_zatwierdz_fv '" + dostawca + "','" + uwagi + "'";
            //MessageBox.Show(proc);
            pol.cmd = new SqlCommand(proc, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            try
            {
                pol.cmd.ExecuteNonQuery();
            }
            catch (SqlException blad)
            {
                if (blad.Errors.Count > 0)
                {
                    MessageBox.Show("Nie można zatwierdzić dokumentu!");
                    return;
                }

            }

            tbCena.Text = "";
            tbIlosc.Text = "";
            tbUwagi.Text = "";
            comboBox2.Text = "";
            comboBoxDostawca.Text = "";

            odswiezenie();
            pol.zakpolaczenieSQL();

            this.Hide();
            dokFv dFv = new dokFv();
            dFv.ShowDialog();
        }

        /// <summary>
        /// Przycisk Wstecz
        /// </summary>
        private void bWstecz_Click(object sender, EventArgs e)
        {
            this.Hide();
            dokFv dFv = new dokFv();
            dFv.ShowDialog();
        }

        /// <summary>
        /// Walidacja wprowadzanych znaków z klawiatury
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void keypress(object sender, KeyPressEventArgs e)
        {
            char litera = e.KeyChar;

            if (!Char.IsDigit(litera) && litera != 8 && litera != 188 && litera != 46 && litera != 44)
            {
                e.Handled = true;
            }
        }

        private void doksprzedpoz_temp_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
