﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;

namespace miniERP
{
    /// <summary>
    ///  Klasa tworzenia nowych dokumentów zakupu PZ.
    /// </summary>
    public partial class nowyPZ : Form
    {
        PolaczenieSQL pol = new PolaczenieSQL();
        string id;

        public nowyPZ()
        {
            InitializeComponent();
        }

        private void nowyPZ_Load(object sender, EventArgs e)
        {
            // TODO: Ten wiersz kodu wczytuje dane do tabeli 'w59018DataSet1.dokdostpoz_temp' . Możesz go przenieść lub usunąć.

            try
            {
                laduj_dane();
            }
            catch
            {

            }


            pol.polaczenieSQL();
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM odbiorca", pol.con);
            DataSet ds = new DataSet(); DataTable dt = new DataTable();
            da.Fill(dt);
            pol.con.Close();
            foreach (DataRow dr in dt.Rows)
            {
                comboBoxDostawca.Items.Add(dr["nazwa"].ToString());
            }


            pol.polaczenieSQL();
            SqlDataAdapter daa = new SqlDataAdapter("SELECT * FROM wytwor WHERE wytwor_idm != ''", pol.con);
            DataSet dss = new DataSet(); DataTable dtt = new DataTable();
            daa.Fill(dtt);
            pol.con.Close();
            foreach (DataRow drr in dtt.Rows)
            {
                comboBox2.Items.Add(drr["wytwor_idm"].ToString());
            }
        }


        public void laduj_dane()
        {
            pol.polaczenieSQL();
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM dokdostpoz_temp", pol.con);
            DataTable lista = new DataTable();
            da.Fill(lista);
            dokdostpoz_tem.DataSource = lista;

            //DataSet ds = new DataSet();
            //da.Fill(ds, "wytwor");
            //listaTow.DataSource = ds;
            //listaTow.DataMember = "wytwor";
            pol.con.Close();

        }


        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxDostawca_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxprodukt(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Odswiezenie danych w oknie
        /// </summary>
        public void odswiezenie()
        {
            pol.polaczenieSQL();
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM dokdostpoz_temp", pol.con);
            DataSet ds = new DataSet();
            da.Fill(ds, "wytwor_idm");
            dokdostpoztempBindingSource1.DataSource = ds;
            dokdostpoztempBindingSource1.DataMember = "wytwor_idm";
            pol.con.Close();

        }

        /// <summary>
        /// Dodawanie pozycji na dokument zakupu
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void dodaj_Click(object sender, EventArgs e)
        {
            if (comboBox2.Text != "" && tbIlosc.Text != "" && tbCena.Text != "")
            {
                if (Convert.ToDecimal(tbCena.Text) == 0)
                {
                    string message = "PODAJ CENĘ ZAKUPU!";
                    string caption = "DODANIE POZYCJI NIE MOŻLIWE";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result;
                    result = MessageBox.Show(message, caption, buttons);
                    return;
                }

                tbCena.Text = tbCena.Text.Replace(',', '.');
                pol.polaczenieSQL();
                pol.cmd = new SqlCommand("INSERT INTO dokdostpoz_temp (wytwor_idm,ilosc,cena,uwagi) " +
                                 "VALUES	(@wytwor_idm,@ilosc,@cena,@uwagi)", pol.con);
                pol.cmd.Parameters.AddWithValue("@wytwor_idm", comboBox2.Text);
                pol.cmd.Parameters.AddWithValue("@ilosc", tbIlosc.Text);
                pol.cmd.Parameters.AddWithValue("@cena", decimal.Parse(tbCena.Text, CultureInfo.InvariantCulture));
                pol.cmd.Parameters.AddWithValue("@uwagi", tbUwagi.Text);
                pol.cmd.CommandType = CommandType.Text;
                pol.cmd.ExecuteNonQuery();

                //string message = "Dodano nową pozycję";
                //string caption = "Dodano pozycję";
                //MessageBoxButtons buttons = MessageBoxButtons.OK;
                //DialogResult result;

                comboBox2.Text = "";
                tbIlosc.Text = "";
                tbCena.Text = "";
                tbUwagi.Text = "";

                //result = MessageBox.Show(message, caption, buttons);

                laduj_dane();
            }
            else
            {
                string message = "Nie wypełniono wymaganych pól: Produkt, Ilość, Cena !";
                string caption = "UWAGA";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(message, caption, buttons);

            }
        }

        private void dokdostpoz_temp(object sender, DataGridViewCellEventArgs e)
        {

        }


        /// <summary>
        /// Usuwanie pozycji z dokumentu
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (id == null)
            {
                MessageBox.Show("Nie zaznaczono pozycji do usunięcia!");
                return;
            }
                pol.polaczenieSQL();
            string query = "DELETE FROM dokdostpoz_temp WHERE dokdostpoz_id = " + id;
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            try
            {
                pol.cmd.ExecuteNonQuery();
            }
            catch (SqlException blad)
            {
                if (blad.Errors.Count > 0)
                {
                    MessageBox.Show("Nie można usunąć pozycji!");
                    return;
                }

            }


            laduj_dane();
            pol.zakpolaczenieSQL();
        }

        /// <summary>
        /// Pobieranie danych z zaznaczonej pozycji na liscie dokumentu
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void dokdostpoz_tem_click(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.dokdostpoz_tem.Rows[e.RowIndex];
            id = row.Cells["dokdostpoz_id"].Value.ToString();

        }

        /// <summary>
        /// Metoda odpowiedzialna za zatwierdzenie dokumentu PZ
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void button1_Click_1(object sender, EventArgs e)
        {
            pol.polaczenieSQL();
            string query = "SELECT COUNT(*) FROM dokdostpoz_temp";
            pol.cmd = new SqlCommand(query, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            pol.cmd.ExecuteNonQuery();
            int zwrotka = (Int32)pol.cmd.ExecuteScalar();

            if (comboBoxDostawca.Text == "")
            {
                MessageBox.Show("Brak dostawcy!");
                return;
            }

            if (zwrotka == 0 )
            {
                MessageBox.Show("Brak pozycji na dokumencie!");
                return;
            }

            
            string dostawca = comboBoxDostawca.Text;
            string uwagi = tbOpisDokumentu.Text;
            string proc = "EXEC up_kpi_zatwierdz_pz '" + dostawca + "','" + uwagi + "'";
            //MessageBox.Show(proc);
            pol.cmd = new SqlCommand(proc, pol.con);
            pol.cmd.CommandType = CommandType.Text;
            try
            {
                pol.cmd.ExecuteNonQuery();
            }
            catch (SqlException blad)
            {
                if (blad.Errors.Count > 0)
                {
                    MessageBox.Show("Nie można zatwierdzić dokumentu!");
                    return;
                }

            }

            tbCena.Text = "";
            tbIlosc.Text = "";
            tbUwagi.Text = "";
            comboBox2.Text = "";
            comboBoxDostawca.Text = "";

            laduj_dane();
            pol.zakpolaczenieSQL();

            this.Hide();
            dokPz dokPz = new dokPz();
            dokPz.ShowDialog();
        }

        /// <summary>
        /// Metoda odpowiedzialna za otworzenie zaznaczonego dokumentu PZ
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.Hide();
            dokPz dokPz = new dokPz();
            dokPz.ShowDialog();
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        /// <summary>
        /// Walidacja wcisnietego przycisku 
        /// </summary>
        /// <param name="sender">obiekt wywołujący  zdarzenie</param>
        /// <param name="e">parametr przysyłający dane do zainicjalizowania zdarzeń w metodzie</param>
        private void keypress(object sender, KeyPressEventArgs e)
        {
            char litera = e.KeyChar;

            if (!Char.IsDigit(litera) && litera != 8 && litera != 188 && litera != 46 && litera != 44)
            {
                e.Handled = true;
            }
        }
    }
}
